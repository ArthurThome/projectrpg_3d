﻿/// Arthur M. Thomé
/// 30 MAY 2020

#region Statements

using UnityEngine;

#endregion

public enum EquipmentSlot
{
    Head,
    Chest,
    Legs,
    Feet,
    Weapon,
    Shield
}

public enum EquipmentMeshRegion
{
    Legs,
    Arms,
    Torso
}

[ CreateAssetMenu ( fileName = "New Equipment", menuName = "Inventory/Equipment" ) ]
public class Equipment : Item
{
    #region Fields

    [ SerializeField ] private SkinnedMeshRenderer m_mesh;

    public EquipmentSlot equipSlot;
    public EquipmentMeshRegion [ ] m_converedMeshRegions; // talvez mudar para private

    [ SerializeField ] private int m_armorModifier;
    [ SerializeField ] private int m_damageModifier;

    #endregion

    #region Properties

    public SkinnedMeshRenderer MMesh { get { return m_mesh; } }

    public int MArmorModifier { get { return m_armorModifier; } }
    public int MDamageModifier { get { return m_damageModifier; } }

    #endregion

    #region Parent Use

    public override void Use ( )
    {
        base.Use ( );
        // Equip the item
        EquipmentManager._Instance.Equip ( this );

        // Remove it from the inventory
        RemoveFromInventory ( );
    }

    #endregion
}
