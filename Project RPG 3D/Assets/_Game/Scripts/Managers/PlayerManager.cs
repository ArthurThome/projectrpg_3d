﻿/// Arthur M. Thomé
/// 01 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

#endregion

public class PlayerManager : MonoBehaviour
{
    #region Singleton

    private static PlayerManager _instance = null;
    public  static PlayerManager _Instance
    {
        get
        {
            if ( _instance == null )
            {
                _instance = FindObjectOfType<PlayerManager> ( );
                if ( _instance == null ) Debug.LogError ( "No Player Manager on scene!" );
            }
            return _instance;
        }
    }

    #endregion

    #region Fields

    [ SerializeField ] private Transform m_playerTrnasform = null;

    [ SerializeField ] private CharacterCombat m_characterCombat;
    [ SerializeField ] private PlayerStats m_playerStats;

    private PlayerBasicStats playerData = new PlayerBasicStats ( );

    #endregion

    #region Properties
    
    public Transform MPlayerTransform { get { return m_playerTrnasform; } }
    public CharacterCombat MCharacterCombat { get { return m_characterCombat; } }
    public PlayerStats MPlayerStats { get { return m_playerStats; } }
    public PlayerBasicStats MPlayerData { get { return playerData; } }

    #endregion

    #region MonoBehaviour Methods

    private void Start ( )
    {
        playerData.LoadPlayerStats ( );
    }

    #endregion

    #region Kill Player

    public void KillPlayer ( )
    {
        // temporario
        playerData.SetCurrentHealth ( playerData.health );

        SceneManager.LoadScene ( SceneManager.GetActiveScene ( ).buildIndex );
    }

    #endregion
}