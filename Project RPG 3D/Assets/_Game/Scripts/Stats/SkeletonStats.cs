﻿/// Arthur M. Thomé
/// 01 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class SkeletonStats : CharacterStats
{
    #region Fields

    private int m_expWhenDie = 50;

    #endregion

    #region Override Die / Take Damage

    public override void TakeDamage ( int damage )
    {
        damage -= MArmor.MCurrentValue;

        base.TakeDamage ( damage );
    }

    public override void Die ( )
    {
        base.Die ( );

        // Add Exp to Player
        PlayerStatsController._Instance.AddExp ( m_expWhenDie );

        // Add ragdoll effect / death animation

        // Add loot

        Destroy ( gameObject );
    }

    #endregion
}