﻿/// Arthur M. Thomé
/// 06 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using TMPro;

#endregion

public class PlayerStatsUI : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private TextMeshProUGUI m_txtHealth;
    [ SerializeField ] private TextMeshProUGUI m_txtStamina;
    [ SerializeField ] private TextMeshProUGUI m_txtIntelligence;
    [ SerializeField ] private TextMeshProUGUI m_txtCraftingSkill;
    [ SerializeField ] private TextMeshProUGUI m_txtFood;
    [ SerializeField ] private TextMeshProUGUI m_txtWater;
    [ SerializeField ] private TextMeshProUGUI m_txtDefense;
    [ SerializeField ] private TextMeshProUGUI m_txtDamage;
    [ SerializeField ] private TextMeshProUGUI m_txtWeight;

    [ SerializeField ] private TextMeshProUGUI m_txtAttributePointsQuantity;

    [ SerializeField ] private Button m_btnHealth;
    [ SerializeField ] private Button m_btnStamina;
    [ SerializeField ] private Button m_btnIntelligence;
    [ SerializeField ] private Button m_btnCraftingSkill;
    [ SerializeField ] private Button m_btnFood;
    [ SerializeField ] private Button m_btnWater;
    [ SerializeField ] private Button m_btnDefense;
    [ SerializeField ] private Button m_btnDamage;
    [ SerializeField ] private Button m_btnWeight;

    [ SerializeField ] private List < Button > m_listButtons = new List < Button > ( );
    
    #endregion

    #region MonoBehaviour Methods

    private void Start ( )
    {
        PlayerStatsController._Instance.onAttributesPointsChanged += SetupPanel;

        SetupPanel ( );
    }

    #endregion

    #region SetupPanel / Update Panel

    public void SetupPanel ( )
    {
        UpdatePanel ( );
        ShowButtons ( );
    }

    private void UpdatePanel ( )
    {
        m_txtAttributePointsQuantity.text = PlayerManager._Instance.MPlayerData.GetAssignmentPoints ( ).ToString ( );

        m_txtHealth.text = "Health ------- " + PlayerManager._Instance.MPlayerData.health;
        m_txtStamina.text = "Stamina ------- " + PlayerManager._Instance.MPlayerData.stamina;
        m_txtIntelligence.text = "Intelligence ------- " + PlayerManager._Instance.MPlayerData.intelligence;
        m_txtCraftingSkill.text = "Crafting Skill ------- " + PlayerManager._Instance.MPlayerData.craftingSkill;
        m_txtFood.text = "Food ------- " + PlayerManager._Instance.MPlayerData.food;
        m_txtWater.text = "Water ------- " + PlayerManager._Instance.MPlayerData.water;
        m_txtDefense.text = "Defense ------- " + PlayerManager._Instance.MPlayerData.defense;
        m_txtDamage.text = "Damage ------- " + PlayerManager._Instance.MPlayerData.attack;
        m_txtWeight.text = "Weight ------- " + PlayerManager._Instance.MPlayerData.weight;
    }

    #endregion

    #region Add Attribute

    public void AddAttributePoints ( string attributeName )
    {
        PlayerManager._Instance.MPlayerData.SetAttribute ( attributeName, 1 );

        //PlayerManager._Instance.MPlayerStats.RecalculateStats ( attributeName );
        UpdatePanel ( );
        ShowButtons ( );

        if ( attributeName == "Health" || attributeName == "Stamina" )
        {
            CanvasUIManager._Instance.infoPlayerUI.OnHealthChanged ( PlayerManager._Instance.MPlayerData.health, PlayerManager._Instance.MPlayerData.GetCurrentHealth ( ) );
        }

        if ( attributeName == "Defense" || attributeName == "Damage" )
        {
            CanvasUIManager._Instance.characterUI.UpdateUI ( );
        }
    }

    #endregion

    #region Show Buttons

    public void ShowButtons ( )
    {
        bool hasPointsToAdd = PlayerStatsController._Instance.GetAssignmentPoints ( ) > 0;

        foreach ( Button btn in m_listButtons )
        {
            btn.interactable = hasPointsToAdd;
        }
    }

    #endregion
}