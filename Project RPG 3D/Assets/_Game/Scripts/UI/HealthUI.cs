﻿/// Arthur M. Thomé
/// 04 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

#endregion

// Mudar ese codigo!!!!!
[ RequireComponent ( typeof ( CharacterStats ) ) ]
public class HealthUI : MonoBehaviour
{
    #region Fields

    public GameObject m_uiPrefab = null;
    public Transform m_target = null;

    [ SerializeField ] private Transform m_ui;
    [ SerializeField ] private Image m_healthSlider;
    [ SerializeField ] private Transform m_cam;

    private float m_visibleTime = 5f;
    private float m_lastMadeVisibleTime;

    #endregion

    #region MonoBehavior Methods

    private void Start ( )
    {
        GetComponent < CharacterStats > ( ).OnHealthChanged += OnHealthChanged;

        m_cam = Camera.main.transform;

        foreach ( Canvas c in FindObjectsOfType < Canvas > ( ) )
        {
            if ( c.renderMode == RenderMode.WorldSpace )
            {
                m_ui = Instantiate ( m_uiPrefab, c.transform ).transform;
                m_healthSlider = m_ui.GetChild ( 0 ).GetComponent < Image > ( );

                m_ui.gameObject.SetActive ( false );
                break;
            }
        }
    }
    
    private void LateUpdate ( )
    {
        if ( m_ui != null )
        {
            m_ui.position = m_target.position;
            m_ui.forward = -m_cam.forward;

            if ( Time.time - m_lastMadeVisibleTime > m_visibleTime )
            {
                m_ui.gameObject.SetActive ( false );
            }
        }
    }

    #endregion

    #region On Health Changed

    private void OnHealthChanged ( int maxHealth, int currentHealth )
    {
        if ( m_ui != null )
        {
            m_ui.gameObject.SetActive ( true );
            m_lastMadeVisibleTime = Time.time;

            float healthPercent = currentHealth / ( float ) maxHealth;

            m_healthSlider.fillAmount = healthPercent;

            if ( currentHealth <= 0 )
            {
                Destroy ( m_ui.gameObject );
            }
        }
    }

    #endregion
}