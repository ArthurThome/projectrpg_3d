﻿/// Arthur M. Thomé
/// 03 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

[ System.Serializable ]
public struct WeaponAnimations
{
    public Equipment weapon;
    public AnimationClip [ ] clips;
}


public class PlayerAnimator : CharacterAnimator
{
    #region Fields

    #region Constants

    //private const float C_ANIMATION_SMOOTH_TIME = 0.1f;

    #endregion

    public WeaponAnimations [ ] weaponAnimations;
    Dictionary < Equipment, AnimationClip [ ] > weaponAnimationsDict;

    #endregion

    #region MonoBehaviour Methods

    protected override void Start ( )
    {
        base.Start ( );

        EquipmentManager._Instance.onEquipmentChanged += OnEquipmentChanged;

        weaponAnimationsDict = new Dictionary < Equipment, AnimationClip [ ] > ( );
        foreach ( WeaponAnimations a in weaponAnimations )
        {
            weaponAnimationsDict.Add ( a.weapon, a.clips );
        }
    }
        
    #endregion

    #region Event Equipment changed

    private void OnEquipmentChanged ( Equipment newItem, Equipment oldItem )
    {
        if ( newItem != null && newItem.equipSlot == EquipmentSlot.Weapon )
        {
            m_animatorController.SetLayerWeight ( 1, 1 );
            if ( weaponAnimationsDict.ContainsKey ( newItem ) )
            {
                m_currentAttackAnimSet = weaponAnimationsDict [ newItem ];
            }
        }
        else if ( newItem == null && oldItem != null && oldItem.equipSlot == EquipmentSlot.Weapon )
        {
            m_animatorController.SetLayerWeight ( 1, 0 );
            m_currentAttackAnimSet = defaultAttackAnimSet;
        }

        if ( newItem != null && newItem.equipSlot == EquipmentSlot.Shield )
        {
            m_animatorController.SetLayerWeight ( 2, 1 );
        }
        else if ( newItem == null && oldItem != null && oldItem.equipSlot == EquipmentSlot.Shield )
        {
            m_animatorController.SetLayerWeight ( 2, 0 );
        }
    }    

    #endregion
}