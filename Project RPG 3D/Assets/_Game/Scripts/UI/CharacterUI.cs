﻿/// Arthur M. Thomé
/// 04 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using TMPro;

#endregion

public class CharacterUI : MonoBehaviour
{
    #region Fields

    #region Constants

    private const string C_CHARACTER_BUTTON = "Character";

    #endregion

    [ SerializeField ] private RectTransform m_panelCharacter = null;

    //[SerializeField] private TextMeshProUGUI m_textHealth = null;
    [ SerializeField ] private TextMeshProUGUI m_textAttack = null;
    [ SerializeField ] private TextMeshProUGUI m_textDeffence = null;

    [ SerializeField ] private PlayerStats m_characterStats = null;

    public CharacterUISlot [ ] m_slotArray; // 1- Head; 2- Chest; 3- Legs; 4- Feet; 5- Sword; 6- Shield;

    #endregion

    #region MonoBehavior Methods

    private void Start ( )
    {
        m_panelCharacter.gameObject.SetActive ( false );

    }

    private void Update ( )
    {
        if ( Input.GetButtonDown ( C_CHARACTER_BUTTON ) )
        {
            m_panelCharacter.gameObject.SetActive ( !m_panelCharacter.gameObject.activeSelf );
        }
    }

    #endregion

    #region Change Equipment Character UI

    public void ChangeEquipmentCharacterUI ( int slot, Equipment equipment = null )
    {
        UpdateUI ( );

        m_slotArray [ slot ].ChangeEquipment ( equipment );
    }

    #endregion

    #region Update UI

    public void UpdateUI ( )
    {
        // Atualizar status
        //m_textHealth.text = m_characterStats.MMaxHealth.ToString ( );
        m_textDeffence.text = PlayerManager._Instance.MPlayerData.GetCurrentDefense ( ).ToString ( );
        m_textAttack.text = PlayerManager._Instance.MPlayerData.GetCurrentAttack ( ).ToString ( );
    }

    #endregion
}