﻿/// Arthur M. Thomé
/// 29 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

#endregion

public class CharacterAnimator : MonoBehaviour
{
    #region Fields

    #region Constants

    private const float C_ANIMATION_SMOOTH_TIME = 0.1f;
    private const string C_PARAMETER_ATTACK = "Attack";
    private const string C_PARAMETER_SPEED_PERCENT = "SpeedPercent";
    private const string C_PARAMETER_IN_COMBAT = "inCombat";

    #endregion

    public AnimationClip replaceableAttackAnim;
    public AnimationClip [ ] defaultAttackAnimSet;
    protected AnimationClip [ ] m_currentAttackAnimSet;

    [ SerializeField ] private NavMeshAgent m_agent = null;
    [ SerializeField ] protected Animator m_animatorController = null;
    [ SerializeField ] protected CharacterCombat m_characterCombat = null;

    [ SerializeField ] protected AnimatorOverrideController m_overrideController;
    
    #endregion

    #region MonoBehavior Methods

    protected virtual void Start ( )
    {
        m_characterCombat.onAttack += OnAttack;

        if ( m_overrideController == null )
        {
            m_overrideController = new AnimatorOverrideController ( m_animatorController.runtimeAnimatorController );
        }
        m_animatorController.runtimeAnimatorController = m_overrideController;

        m_currentAttackAnimSet = defaultAttackAnimSet;
    }
    
    protected virtual void Update ( )
    {
        float speedPercent = m_agent.velocity.magnitude / m_agent.speed;

        m_animatorController.SetFloat ( C_PARAMETER_SPEED_PERCENT, speedPercent, C_ANIMATION_SMOOTH_TIME, Time.deltaTime );

        m_animatorController.SetBool ( C_PARAMETER_IN_COMBAT, m_characterCombat.MInCombat );
    }

    #endregion

    #region On Attack

    protected virtual void OnAttack ( )
    {
        m_animatorController.SetTrigger ( C_PARAMETER_ATTACK );
        int attackIndex = Random.Range ( 0, m_currentAttackAnimSet.Length );
        m_overrideController [ replaceableAttackAnim.name ] = m_currentAttackAnimSet [ attackIndex ];
    }

    #endregion
}