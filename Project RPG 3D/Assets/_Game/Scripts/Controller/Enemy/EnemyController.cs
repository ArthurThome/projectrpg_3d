﻿/// Arthur M. Thomé
/// 01 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

#endregion

public class EnemyController : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private float m_lookRadius = 10;

    [ SerializeField ] private NavMeshAgent m_agent;
    [ SerializeField ] private CharacterCombat m_combat;
    private Transform m_target;


    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        m_target = PlayerManager._Instance.MPlayerTransform;
    }
    
    void Update ( )
    {
        
        float distance = Vector3.Distance ( m_target.position, transform.position );

        if ( distance <= m_lookRadius )
        {
            m_agent.SetDestination ( m_target.position );

            if ( distance <= m_agent.stoppingDistance )
            {
                CharacterStats targetStats = m_target.GetComponent < CharacterStats > ( );

                if ( targetStats != null )
                {
                    // Attack the target
                    m_combat.Attack ( targetStats );
                }

                // Face the target
                FaceTarget ( );
            }
        }

        if ( distance > m_lookRadius )
        {
            m_agent.SetDestination ( m_agent.transform.position );
        }
    }

    #endregion

    #region Face Target

    private void FaceTarget ( )
    {
        Vector3 direction = ( m_target.position - transform.position ).normalized;
        Quaternion lookRotation = Quaternion.LookRotation ( new Vector3 ( direction.x, 0f, direction.z ) );
        transform.rotation = Quaternion.Slerp ( transform.rotation, lookRotation, Time.deltaTime * 5f );
    }

    #endregion

    #region Gizmos

    private void OnDrawGizmosSelected ( )
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere ( transform.position, m_lookRadius);
    }

    #endregion
}