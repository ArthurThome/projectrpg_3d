﻿/// Arthur M. Thomé
/// 06 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class CanvasUIManager : MonoBehaviour
{
    #region Singleton

    private static CanvasUIManager _instance = null;
    public  static CanvasUIManager _Instance
    {
        get
        {
            if ( _instance == null )
            {
                _instance = FindObjectOfType < CanvasUIManager > ( );
                if ( _instance == null ) Debug.LogError ( "No Canvas UI Manager on scene!" );
            }
            return _instance;
        }
    }

    #endregion

    #region Fields

    public PlayerStatsUI playerStatsUI;
    public InfoPlayerUI infoPlayerUI;
    public CharacterUI characterUI;

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        
    }
    
    void Update ( )
    {
        
    }

    #endregion
}