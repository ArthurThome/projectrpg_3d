﻿/// Arthur M. Thomé
/// 01 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

[ RequireComponent ( typeof ( CharacterStats ) ) ]
public class CharacterCombat : MonoBehaviour
{
    #region Fields

    #region Constants
    
    private const float C_COMBAT_COOLDOWN = 5f;

    #endregion

    [ SerializeField ] private float m_attackSpeed = 1f;
    private float m_attackCooldown = 0f;
    private float m_lastAttackTime = 0f;

    private float m_attackDelay = 0.6f;

    private CharacterStats m_myStats = null;
    private CharacterStats m_opponentStats = null;

    private bool m_inCombat = false;

    #endregion

    #region Properties

    public bool MInCombat { get { return m_inCombat; } }

    #endregion

    #region Delegate

    public delegate void OnAttack ( );
    public OnAttack onAttack;

    #endregion

    #region MonoBehaviour Methods

    private void Start ( )
    {
        m_myStats = GetComponent < CharacterStats > ( );
    }

    private void Update ( )
    {
        m_attackCooldown -= Time.deltaTime;

        if ( Time.time - m_lastAttackTime > C_COMBAT_COOLDOWN )
        {
            m_inCombat = false;
        }
    }

    #endregion

    #region Attack

    public void Attack ( CharacterStats targetStats )
    {
        if ( m_attackCooldown <= 0f )
        {
            //StartCoroutine ( DoDamage ( targetStats, m_attackDelay ) );
            m_opponentStats = targetStats;

            onAttack?.Invoke ( );

            m_attackCooldown = 1f / m_attackSpeed;
            m_inCombat = true;
            m_lastAttackTime = Time.time;
        }
    }

    #endregion

    #region Enumerator Do Damage

    private IEnumerator DoDamage ( CharacterStats stats, float delay )
    {
        yield return new WaitForSeconds ( delay );

        stats.TakeDamage ( PlayerManager._Instance.MPlayerStats.MPlayerCurrentBasicStats.GetCurrentAttack ( ) );

        if ( stats.MCurrentHealth <= 0 )
        {
            m_inCombat = false;
        }
    }

    #endregion

    #region Attack Hit Animation Event

    public void AttackHit_AnimationEvent ( )
    {
        switch ( m_opponentStats.tag )
        {
            case "Player":
                m_opponentStats.TakeDamage ( m_myStats.MDamage.MCurrentValue );
                break;
            case "Enemy":
                m_opponentStats.TakeDamage ( PlayerManager._Instance.MPlayerStats.MPlayerCurrentBasicStats.GetCurrentAttack ( ) );
                break;
        }


        if ( m_opponentStats.MCurrentHealth <= 0 )
        {
            m_inCombat = false;
        }
    }

    #endregion
}