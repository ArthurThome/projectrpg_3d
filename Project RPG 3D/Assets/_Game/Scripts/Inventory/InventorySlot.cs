﻿/// Arthur M. Thomé
/// 30 MAY 2020

#region Statements

using UnityEngine;
using UnityEngine.UI;

#endregion

public class InventorySlot : MonoBehaviour
{
    #region Fields

    [SerializeField] private Button m_removeButton = null;
    [SerializeField] private Image m_removeButtonSprite = null;
    [SerializeField] private Image m_icon = null;

    private Item m_item = null;

    #endregion

    #region Add Item

    public void AddItem ( Item newItem )
    {
        m_item = newItem;

        m_icon.sprite = m_item.icon;
        m_icon.enabled = true;

        m_removeButton.interactable = true;
        m_removeButtonSprite.enabled = true;
    }

    public void ClearSlot ( )
    {
        m_item = null;

        m_icon.sprite = null;
        m_icon.enabled = false;

        m_removeButton.interactable = false;
        m_removeButtonSprite.enabled = false;
    }

    #endregion

    #region OnRemoveButton

    public void OnRemoveButton ( )
    {
        GameManager._Instance.MInventory.RemoveItemFromInventory ( m_item );
    }

    #endregion

    #region Use Item

    public void UseItem ( )
    {
        if ( m_item != null )
        {
            m_item.Use ( );
        }
    }

    #endregion
}