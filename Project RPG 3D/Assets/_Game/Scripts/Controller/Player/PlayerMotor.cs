﻿/// Arthur M. Thomé
/// 29 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

#endregion

[ RequireComponent ( typeof ( NavMeshAgent ) ) ]
public class PlayerMotor : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private NavMeshAgent m_agent;    //reference to our player
    
    private Transform m_target;     // target to follow

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        
    }
    
    void Update ( )
    {
        if ( m_target != null )
        {
            m_agent.SetDestination ( m_target.position );
            FaceTarget ( );
        }
    }

    #endregion

    #region Move To Point

    public void MoveToPoint ( Vector3 point )
    {
        m_agent.SetDestination ( point );
    }

    #endregion

    #region Follow / Stop Target

    public void FollowTarget ( Interactable newTarget )
    {
        m_agent.stoppingDistance = newTarget.MRadius * 0.8f;
        m_agent.updateRotation = false;

        m_target = newTarget.MInteractionTransform;
    }

    public void StopFollowingTarget ( )
    {
        m_agent.stoppingDistance = 0f;
        m_agent.updateRotation = true;

        m_target = null; 
    }

    #endregion

    private void FaceTarget ( )
    {
        Vector3 direction = ( m_target.position - transform.position ).normalized;
        Quaternion lookRotation = Quaternion.LookRotation ( new Vector3 ( direction.x, 0f, direction.z ) );
        transform.rotation = Quaternion.Slerp ( transform.rotation, lookRotation, Time.deltaTime * 5f );
    }
}