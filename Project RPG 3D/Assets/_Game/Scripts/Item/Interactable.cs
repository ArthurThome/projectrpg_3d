﻿/// Arthur M. Thomé
/// 29 MAY 2020

#region Statements

using UnityEngine;

#endregion

public class Interactable : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private float m_radius = 1.5f;

    [ SerializeField ] private Transform m_interactionTransform = null;

    public bool m_isFocus = false;
    private Transform m_player = null;

    private bool m_hasInteract = false;

    #endregion

    #region Properties

    public float MRadius { get { return m_radius; } }
    public Transform MInteractionTransform { get { return m_interactionTransform; } }

    #endregion

    #region MonoBehaviours Methods

    private void Update ( )
    {
        if ( m_isFocus && !m_hasInteract )
        {
            float distance = Vector3.Distance ( m_player.position, m_interactionTransform.position );
            if ( distance <= m_radius )
            {
                Interact ( );
                m_hasInteract = true;
            }
        }
    }

    #endregion

    #region virtual Interact

    public virtual void Interact ( )
    {

    }

    #endregion

    #region Focused / Defocused

    public void OnFocused ( Transform playerTransform )
    {
        m_isFocus = true;
        m_player = playerTransform;
        m_hasInteract = false;
    }

    public void OnDefocused ( )
    {
        m_isFocus = false;
        m_player = null;
        m_hasInteract = false;
    }

    #endregion

    #region OnDrawGizmos

    public void OnDrawGizmos ( )
    {
        if ( m_interactionTransform == null )
            m_interactionTransform = transform;

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere ( m_interactionTransform.position, m_radius );
    }

    #endregion
}