﻿/// Arthur M. Thomé
/// 01 JUN 2020

#region Statements

using UnityEngine;

#endregion

public class CharacterStats : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private int m_maxHealth = 100;
    [ SerializeField ] private int m_currentHealth = 0;
    
    [ SerializeField ] protected Stat damage;
    [ SerializeField ] protected Stat armor;

    public event System.Action < int, int > OnHealthChanged;

    #endregion

    #region Properties

    public int MMaxHealth { get { return m_maxHealth; } }
    public int MCurrentHealth { get { return m_currentHealth; } }

    public Stat MDamage { get { return damage; } }
    public Stat MArmor { get { return armor; } }

    #endregion

    #region MonoBehavior Methods

    private void Awake ( ) 
    {
        m_currentHealth = m_maxHealth;
    }

    private void Update ( )
    {
        
    }

    #endregion

    #region Take Damage / Die

    public virtual void TakeDamage ( int damage )
    {        
        damage = Mathf.Clamp ( damage, 0, int.MaxValue );

        m_currentHealth -= damage;
        Debug.Log ( transform.name + " takes " + damage + " damage." );

        OnHealthChanged?.Invoke ( m_maxHealth, m_currentHealth );

        if ( m_currentHealth <= 0 )
        {
            Die ( );
        }
    }

    public virtual void Die ( )
    {
        // Die in some way
        // This method is meant to be overwritten
        Debug.Log ( transform.name + " died." );
    }

    #endregion
}