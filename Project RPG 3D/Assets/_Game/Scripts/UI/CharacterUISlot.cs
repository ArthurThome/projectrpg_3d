﻿/// Arthur M. Thomé
/// 04 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

#endregion

public class CharacterUISlot : MonoBehaviour
{
    #region Fields

    [SerializeField] private Button m_Button = null;
    [SerializeField] private Image m_icon = null;

    private Equipment m_item = null;

    #endregion

    #region MonoBehavior Methods

    public void ChangeEquipment ( Equipment equip )
    {
        if ( equip != null )
        {
            m_item = equip;

            m_icon.sprite = m_item.icon;
        }
        else
        {
            m_item = null;
            m_icon.sprite = null;
        }
    }

    #endregion

    #region Unequip Item

    public void UnequipItem ( )
    {
        EquipmentManager._Instance.Unequip ( ( int ) m_item.equipSlot );
    }

    #endregion
}