﻿/// Arthur M. Thomé
/// 01 JUN 2020

#region Statements

using System.Collections.Generic;

using UnityEngine;

#endregion

 [ System.Serializable ]
public class Stat
{
    #region Fields

    [ SerializeField ] private int m_currentValue = 0;

    private List < int > m_modifiers = new List<int> ( );

    #endregion

    #region Properties

    public int MCurrentValue { get { return m_currentValue; } }

    #endregion

    #region Add / Remove Modifier

    public void AddModifier ( int modifier )
    {
        if ( modifier != 0 )
        {
            m_modifiers.Add ( modifier );
            m_currentValue += modifier;
        }
    }

    public void RemoveModifier ( int modifier )
    {
        if ( modifier != 0 )
        {
            m_modifiers.Remove ( modifier );
            m_currentValue -= modifier;
        }
    }

    #endregion

    #region GetModifiers 

    //public int GetCurrent

    #endregion
}