﻿/// Arthur M. Thomé
/// 29 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class CameraController : MonoBehaviour
{
    #region Fields

    [ Header ( "Camera Controller" ) ]
    [ SerializeField ] private Transform m_target = null;

    [ SerializeField ] private Vector3 m_offset = Vector3.zero;

    [ SerializeField ] private float m_pitch = 2f;
    [ SerializeField ] private float m_currentZoom = 10f;

    [ Header ( "Zoom Controller" ) ]
    [ SerializeField ] private float m_zoomSpeed = 4f;
    [ SerializeField ] private float m_minZoom = 5f;
    [ SerializeField ] private float m_maxZoom = 15f;

    [ Header ( "Yaw Controller" ) ]
    [ SerializeField ] private float m_yawSpeed = 100f;
    [ SerializeField ] private float m_currentYaw = 0f;
     
    #endregion

    #region MonoBehavior Methods

    private void Update ( )
    {
        // Zoom on camera
        m_currentZoom -= Input.GetAxis ( "Mouse ScrollWheel" ) * m_zoomSpeed;
        m_currentZoom = Mathf.Clamp ( m_currentZoom, m_minZoom, m_maxZoom );
        
        // Yaw on camera
        m_currentYaw -= Input.GetAxis ( "Horizontal" ) * m_yawSpeed * Time.deltaTime;
    }

    private void LateUpdate ( )
    {
        //Camera follow the player
        transform.position = m_target.position - m_offset * m_currentZoom;
        transform.LookAt ( m_target.position + Vector3.up * m_pitch );

        transform.RotateAround ( m_target.position, Vector3.up, m_currentYaw );
    }

    #endregion
}