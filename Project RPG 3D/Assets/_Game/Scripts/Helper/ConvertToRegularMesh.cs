﻿/// Arthur M. Thomé
/// 31 MAY 2020

#region Statements

using UnityEngine;

#endregion

public class ConvertToRegularMesh : MonoBehaviour
{

    #region Convert

    [ ContextMenu ( "Convert to regular mesh" ) ]
    private void Convert ( )
    {
        SkinnedMeshRenderer skinnedMeshRenderer = GetComponent < SkinnedMeshRenderer > ( );
        MeshRenderer meshRenderer = gameObject.AddComponent < MeshRenderer > ( );
        MeshFilter meshFilter = gameObject.AddComponent < MeshFilter > ( );

        meshFilter.sharedMesh = skinnedMeshRenderer.sharedMesh;
        meshRenderer.sharedMaterials = skinnedMeshRenderer.sharedMaterials;

        DestroyImmediate ( skinnedMeshRenderer );
        DestroyImmediate ( this );
    }

    #endregion
}