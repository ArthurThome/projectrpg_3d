﻿/// Arthur M. Thomé
/// 29 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class GameManager : MonoBehaviour
{

    #region Singleton

    private static GameManager _instance = null;
    public  static GameManager _Instance
    {
        get
        {
            if ( _instance == null )
            {
                _instance = FindObjectOfType<GameManager> ( );
                if ( _instance == null ) Debug.LogError ( "No Game Manager on scene!" );
            }
            return _instance;
        }
    }

    #endregion

    #region Fields

    [ SerializeField ] private PlayerMotor m_playerMotor = null;
    [ SerializeField ] private Inventory m_inventory = null;

    #endregion

    #region Properties

    public PlayerMotor MPlayerMotor { get { return m_playerMotor; } }
    public Inventory MInventory { get { return m_inventory; } }

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {

    }
    
    void Update ( )
    {
        
    }

    #endregion
}
