﻿/// Arthur M. Thomé
/// 01 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class PlayerStats : CharacterStats
{
    #region Fields

    //public TypeCharacter typeCharacter;

    //criar o basic
    [SerializeField] private PlayerBasicStats m_playerBasicStats;

    [SerializeField] private PlayerBasicStats m_playerCurrentBasicStats;

    #endregion

    #region Properties

    public PlayerBasicStats MPlayerCurrentBasicStats { get { return m_playerCurrentBasicStats; } }
    public PlayerBasicStats MPlayerBasicStats { get { return m_playerBasicStats; } }

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        EquipmentManager._Instance.onEquipmentChanged += OnEquipmentChanged;
        //PlayerStatsController._Instance.onAttributesPointsChanged += CalculateStats;

        CalculateStats ( );
        //temporatio
        CanvasUIManager._Instance.characterUI.UpdateUI ( );
    }

    #endregion

    #region Override Die / Take Damage

    public override void TakeDamage(int damage)
    {
        damage -= m_playerCurrentBasicStats.GetCurrentDefense();

        base.TakeDamage(damage);
    }

    public override void Die()
    {
        base.Die();

        // Kill the player
        PlayerManager._Instance.KillPlayer();
    }

    #endregion

    #region On Equipment Changed

    private void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
    {
        if (newItem != null)
        {
            armor.AddModifier(newItem.MArmorModifier);
            damage.AddModifier(newItem.MDamageModifier);

            m_playerCurrentBasicStats.SetCurrentAttack(damage.MCurrentValue);
            m_playerCurrentBasicStats.SetCurrentDefense(armor.MCurrentValue);
        }

        if (oldItem != null)
        {
            armor.RemoveModifier(oldItem.MArmorModifier);
            damage.RemoveModifier(oldItem.MDamageModifier);

            m_playerCurrentBasicStats.SetCurrentAttack(damage.MCurrentValue);
            m_playerCurrentBasicStats.SetCurrentDefense(armor.MCurrentValue);
        }
    }

    #endregion

    #region CalculeStats / Recalculade

    private void CalculateStats ( )
    {
        PlayerManager._Instance.MPlayerData.health += PlayerManager._Instance.MPlayerData.GetAttribute ( "Health" ) * 10;
        PlayerManager._Instance.MPlayerData.stamina += PlayerManager._Instance.MPlayerData.GetAttribute ( "Stamina" ) * 5;
        PlayerManager._Instance.MPlayerData.intelligence += PlayerManager._Instance.MPlayerData.GetAttribute ( "Intelligence" );
        PlayerManager._Instance.MPlayerData.craftingSkill += PlayerManager._Instance.MPlayerData.GetAttribute ( "CraftingSkill" );
        PlayerManager._Instance.MPlayerData.food += PlayerManager._Instance.MPlayerData.GetAttribute ( "Food" ) * 5;
        PlayerManager._Instance.MPlayerData.water += PlayerManager._Instance.MPlayerData.GetAttribute ( "Water" ) * 5;
        PlayerManager._Instance.MPlayerData.defense += PlayerManager._Instance.MPlayerData.GetAttribute ( "Defense" );
        PlayerManager._Instance.MPlayerData.attack += PlayerManager._Instance.MPlayerData.GetAttribute ( "Damage" );
        PlayerManager._Instance.MPlayerData.weight += PlayerManager._Instance.MPlayerData.GetAttribute ( "Weight" ) * 10;
    }

    public void RecalculateStats ( string attributeName )
    {
        switch ( attributeName )
        {
            case "Health":
                m_playerCurrentBasicStats.health = m_playerBasicStats.health + PlayerStatsController._Instance.GetAttributePoints ( "Health" ) * 10;
                break;
            case "Stamina":
                m_playerCurrentBasicStats.stamina = m_playerBasicStats.stamina + PlayerStatsController._Instance.GetAttributePoints ( "Stamina" ) * 5;
                break;
            case "Intelligence":
                m_playerCurrentBasicStats.intelligence = m_playerBasicStats.intelligence + PlayerStatsController._Instance.GetAttributePoints ( "Intelligence" );
                break;
            case "CraftingSkill":
                m_playerCurrentBasicStats.craftingSkill = m_playerBasicStats.craftingSkill + PlayerStatsController._Instance.GetAttributePoints ( "CraftingSkill" );
                break;
            case "Food":
                m_playerCurrentBasicStats.food = m_playerBasicStats.food + PlayerStatsController._Instance.GetAttributePoints ( "Food" ) * 5;
                break;
            case "Water":
                m_playerCurrentBasicStats.water = m_playerBasicStats.water + PlayerStatsController._Instance.GetAttributePoints ( "Water" ) * 5;
                break;
            case "Defense":
                m_playerCurrentBasicStats.defense = m_playerBasicStats.defense + PlayerStatsController._Instance.GetAttributePoints ( "Defense" );
                break;
            case "Damage":
                m_playerCurrentBasicStats.attack = m_playerBasicStats.attack + PlayerStatsController._Instance.GetAttributePoints ( "Damage" );
                break;
            case "Weight":
                m_playerCurrentBasicStats.weight = m_playerBasicStats.weight + PlayerStatsController._Instance.GetAttributePoints ( "Weight" ) * 10;
                break;
        }
    }

    #endregion
}