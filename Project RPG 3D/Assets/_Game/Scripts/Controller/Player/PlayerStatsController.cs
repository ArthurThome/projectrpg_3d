﻿/// Arthur M. Thomé
/// 05 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

//[ System.Serializable ]
//public class BasicInfoChar
//{
//    public PlayerBasicStats baseInfo;
//    public TypeCharacter typeChar;
//}

public class PlayerStatsController : MonoBehaviour
{
    #region Singleton

    private static PlayerStatsController _instance = null;
    public  static PlayerStatsController _Instance
    {
        get
        {
            if ( _instance == null )
            {
                _instance = FindObjectOfType<PlayerStatsController> ( );
                if ( _instance == null ) Debug.LogError ( "No Player Stats Controller on scene!" );
            }
            return _instance;
        }
    }

    #endregion

    #region Fields

    #region Constants

    private const string                    C_SCENE_GAME_PLAY       = "GamePlay";       //Nome da cena de game play.

    private const string                    C_PP_CURRENT_EXP        = "CurrentEXP";     //Player Prefs experiencia atual.
    private const string                    C_PP_CURRENT_LEVEL      = "CurrentLevel";   //Player Prefs nivel atual.
    private const string                    C_PP_TYPE_CHARACTER     = "TypeCharacter";
    private const string                    C_PP_ASSIGNMENT_POINTS  = "AssignmentPoints";
    private const string                    C_PP_ATTRIBUTE          = "Attribute";      // Concatenar com o nome do atributo

    #endregion

    [ Header ( "Experience" ) ] 
    [ SerializeField ] private int          m_expMultiply       = 1;                //Multiplicador de experiencia.
    [ SerializeField ] private float        m_expFirstLevel     = 100;              //Base para somatoria de experiencia para proxima nivel.
    [ SerializeField ] private float        m_expNextLevel      = 0;                //Quantidade de experiencia para proxima nivel.

    [ Header ( "Difficulty" ) ]
    [ SerializeField ] private float        m_difficultyFactor  = 1.5f;             //Fator de dificuldade para upar de nivel.

    [ SerializeField ] private int m_attributesLevelingUp = 5;
    [ SerializeField ] private int m_attributesToRemoveLevelingUp = 1;

    //[ SerializeField ] private List<BasicInfoChar> m_baseInfoChars;

    #endregion

    #region Delegate

    public delegate void OnExpChanged ( int playerLevel, float currentExp, float nextLevelExp );
    public OnExpChanged onExpChanged;

    public delegate void OnAttributesPointsChanged ( );
    public OnAttributesPointsChanged onAttributesPointsChanged;
    
    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        //DontDestroyOnLoad ( this );
        //SceneManager.LoadScene ( C_SCENE_GAME_PLAY );
      
        m_expNextLevel = GetNextLevelExp ( );
    }
    
    void Update ( )
    {
        if ( Input.GetKeyDown ( KeyCode.L ) )
        {
            //Clear Player Prefs
            PlayerPrefs.DeleteKey ( C_PP_CURRENT_LEVEL );
            PlayerPrefs.DeleteKey ( C_PP_CURRENT_EXP );

            onExpChanged.Invoke ( PlayerManager._Instance.MPlayerData.GetCurrentLevel ( ), PlayerManager._Instance.MPlayerData.GetCurrentExp ( ), GetNextLevelExp ( ) );
        }

        if ( Input.GetKeyDown ( KeyCode.K ) )
        {
            //Clear Player Prefs
            PlayerPrefs.DeleteKey ( C_PP_ASSIGNMENT_POINTS );
            PlayerPrefs.DeleteKey ( C_PP_ATTRIBUTE + "Health" );
            PlayerPrefs.DeleteKey ( C_PP_ATTRIBUTE + "Stamina" );
            PlayerPrefs.DeleteKey ( C_PP_ATTRIBUTE + "Intelligence" );
            PlayerPrefs.DeleteKey ( C_PP_ATTRIBUTE + "CraftingSkill" );
            PlayerPrefs.DeleteKey ( C_PP_ATTRIBUTE + "Food" );
            PlayerPrefs.DeleteKey ( C_PP_ATTRIBUTE + "Water" );
            PlayerPrefs.DeleteKey ( C_PP_ATTRIBUTE + "Defense" );
            PlayerPrefs.DeleteKey ( C_PP_ATTRIBUTE + "Damage" );
            PlayerPrefs.DeleteKey ( C_PP_ATTRIBUTE + "Weight" );

            onAttributesPointsChanged.Invoke ( );
        }

        //if ( Input.GetKeyDown ( KeyCode.T ) )
        //{
        //    //Clear Player Prefs
        //    PlayerPrefs.DeleteKey ( C_PP_TYPE_CHARACTER );
        //}

        if ( Input.GetKeyDown ( KeyCode.G ) )
        {
            AddExp ( 1000 );
        }

        if ( Input.GetKeyDown ( KeyCode.B ) )
        {
            Debug.Log ( "Level: " + PlayerManager._Instance.MPlayerData.GetCurrentLevel ( ) );
            Debug.Log ( "EXP: " + PlayerManager._Instance.MPlayerData.GetCurrentExp ( ) );
        }
    }

    #endregion

    #region Add EXP / Level 

    public void AddExp ( float exp )
    {
        float newEXP = PlayerManager._Instance.MPlayerData.GetCurrentExp ( ) + ( exp * m_expMultiply );

        while ( newEXP >= GetNextLevelExp ( ) ) 
        {
            newEXP -= GetNextLevelExp ( );
            AddLevel ( );
        } 

        //PlayerPrefs.SetFloat ( C_PP_CURRENT_EXP, newEXP );
        PlayerManager._Instance.MPlayerData.SetCurrentEXP ( newEXP );
        
        onExpChanged.Invoke ( PlayerManager._Instance.MPlayerData.GetCurrentLevel ( ), PlayerManager._Instance.MPlayerData.GetCurrentExp ( ), GetNextLevelExp ( ) );
    }
    
    private void AddLevel ( )
    {
        int newLevel = PlayerManager._Instance.MPlayerData.GetCurrentLevel ( ) + 1;
        //PlayerPrefs.SetInt ( C_PP_CURRENT_LEVEL, newLevel );
        PlayerManager._Instance.MPlayerData.SetCurrentLevel ( newLevel );

        //SetAssignmentPoints ( GetAssignmentPoints ( ) + m_attributesLevelingUp );
        PlayerManager._Instance.MPlayerData.SetAssignmentPoints ( PlayerManager._Instance.MPlayerData.GetAssignmentPoints ( ) + m_attributesLevelingUp );
    }

    #endregion

    #region Get

    public float GetNextLevelExp ( )
    {
        return m_expNextLevel = m_expFirstLevel * ( PlayerManager._Instance.MPlayerData.GetCurrentLevel ( ) + 1 ) * m_difficultyFactor;
    }
       
    #endregion

    #region Get/Set Add / Remove Player Prefs

    //public float GetCurrentExp ( )
    //{
    //    return PlayerPrefs.GetFloat ( C_PP_CURRENT_EXP );
    //}

    //public int GetCurrentLevel ( )
    //{
    //    return PlayerPrefs.GetInt ( C_PP_CURRENT_LEVEL );
    //}  
    
    //public int GetAssignmentPoints ( )
    //{
    //    return PlayerPrefs.GetInt ( C_PP_ASSIGNMENT_POINTS );
    //}

    //public void SetAssignmentPoints ( int points )
    //{
    //    PlayerPrefs.SetInt ( C_PP_ASSIGNMENT_POINTS, points );

    //    onAttributesPointsChanged.Invoke ( );
    //}

    //public int GetAttributePoints ( string attributeName )
    //{
    //    return PlayerPrefs.GetInt ( C_PP_ATTRIBUTE + attributeName );
    //}

    public void AddAttributePoints ( string attributeName, int value )
    {
        if ( PlayerManager._Instance.MPlayerData.GetAssignmentPoints ( ) > 0 )
        {
            //CRIAR UM DOCIONARIO PARA GUARDAR ESSES PONTOS!!!

            //Debug.Log(C_PP_ATTRIBUTE + attributeName);
            //PlayerPrefs.SetInt ( C_PP_ATTRIBUTE + attributeName, GetAttributePoints ( attributeName ) + value );
            PlayerManager._Instance.MPlayerData.SetAttribute ( attributeName, value );

            
            PlayerManager._Instance.MPlayerData.SetAssignmentPoints ( PlayerManager._Instance.MPlayerData.GetAssignmentPoints ( ) - m_attributesToRemoveLevelingUp );

            if ( onAttributesPointsChanged != null )
                onAttributesPointsChanged.Invoke ( );
        }
    }

    //public TypeCharacter GetTypeCharacter ( )
    //{
    //    switch ( PlayerPrefs.GetString ( C_PP_TYPE_CHARACTER ) )
    //    {
    //        case "SURVIVALIST":
    //            return TypeCharacter.SURVIVALIST;
    //        case "ENGINEER":
    //            return TypeCharacter.ENGINEER;
    //        case "FIGHTER":
    //            return TypeCharacter.FIGHTER;
    //        case "NERD":
    //            return TypeCharacter.NERD;
    //    }

    //    return TypeCharacter.SURVIVALIST;
    //}

    //public void SetTypeCharacter ( TypeCharacter type )
    //{
    //    PlayerPrefs.SetString ( C_PP_TYPE_CHARACTER, type.ToString ( ) );
    //}

    
    #endregion

    //public PlayerBasicStats GetBasicStats ( )
    //{
    //    foreach ( PlayerBasicStats basic in m_baseInfoChars )
    //    {
    //        if ( basic.typeChar == type )
    //        {
    //            return basic.baseInfo;
    //        }
    //    }

    //    //apenas pq precisa de um return, melhorar depois
    //    return m_baseInfoChars [ 0 ].baseInfo;
    //}
}