﻿/// Arthur M. Thomé
/// 04 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class CharacterAnimationEventReceiver : MonoBehaviour
{
    #region Fields

    public CharacterCombat m_combat;

    #endregion

    #region Attack Event

    public void AttackHitEvent ( )
    {
        m_combat.AttackHit_AnimationEvent ( );
    }

    #endregion
}
