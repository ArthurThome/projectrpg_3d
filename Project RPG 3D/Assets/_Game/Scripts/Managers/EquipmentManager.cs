﻿/// Arthur M. Thomé
/// 30 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class EquipmentManager : MonoBehaviour
{
    #region Singleton

    private static EquipmentManager _instance = null;
    public  static EquipmentManager _Instance
    {
        get
        {
            if ( _instance == null )
            {
                _instance = FindObjectOfType<EquipmentManager> ( );
                if ( _instance == null ) Debug.LogError ( "No Equipment Manager on scene!" );
            }
            return _instance;
        }
    }

    #endregion

    #region Fields

    public Equipment [ ] m_defaultItems;
    public SkinnedMeshRenderer m_targetMesh = null;

    private Equipment [ ] m_currentEquipment;
    private SkinnedMeshRenderer [ ] m_currentMeshes;

    public CharacterUI characterUI = null;

    #endregion

    #region Delegate

    public delegate void OnEquipmentChanged ( Equipment newItem, Equipment oldItem );
    public OnEquipmentChanged onEquipmentChanged;

    #endregion

    #region MonoBehavior Methods

    private void Start ( )
    {
        int numSlots = System.Enum.GetNames ( typeof ( EquipmentSlot ) ).Length;
        m_currentEquipment = new Equipment [ numSlots ];
        m_currentMeshes = new SkinnedMeshRenderer [ numSlots];

        EquipDefaultItems ( );
    }

    private void Update ( )
    {

        // implementar apenas quando o inventario estiver aberto
        if ( Input.GetKeyDown ( KeyCode.U ) )
            UnequipAll ( );
    }

    #endregion

    #region Equip / Unequip

    public void Equip ( Equipment newItem )
    {
        int slotIndex = ( int ) newItem.equipSlot;
               
        // Switch equipments
        Equipment oldItem = Unequip ( slotIndex );
               
        if ( onEquipmentChanged != null )
        {
            onEquipmentChanged.Invoke ( newItem, oldItem );
        }

        characterUI.ChangeEquipmentCharacterUI ( slotIndex, newItem );

        SetEquipmentBlendShapes ( newItem, 100 );

        m_currentEquipment [ slotIndex ] = newItem;

        SkinnedMeshRenderer newMesh = Instantiate ( newItem.MMesh );
        newMesh.transform.parent = m_targetMesh.transform;

        newMesh.bones = m_targetMesh.bones;
        newMesh.rootBone = m_targetMesh.rootBone;
        m_currentMeshes [ slotIndex ] = newMesh;
    }

    public Equipment Unequip ( int slotIndex )
    {
        if ( m_currentEquipment [ slotIndex ] != null )
        {
            if ( m_currentMeshes [ slotIndex ] != null )
            {
                Destroy ( m_currentMeshes [ slotIndex ].gameObject );
            }

            Equipment oldItem = m_currentEquipment [ slotIndex ];

            SetEquipmentBlendShapes ( oldItem, 0 );

            GameManager._Instance.MInventory.AddItemToInventory ( oldItem );

            m_currentEquipment [ slotIndex ] = null;

            if ( onEquipmentChanged != null )
            {
                onEquipmentChanged.Invoke ( null, oldItem );
            }

            characterUI.ChangeEquipmentCharacterUI ( slotIndex );

            if ( slotIndex < m_defaultItems.Length )
                EquipDefaultItem ( slotIndex );

            return oldItem;
        }

        return null;
    }

    public void UnequipAll ( )
    {
        for (int i = 0; i < m_currentEquipment.Length; i++)
        {
            Unequip ( i );
        }

        EquipDefaultItems ( );
    }

    #endregion

    #region Set Equipment Blend Shapes

    private void SetEquipmentBlendShapes ( Equipment item, int weight )
    {
        foreach ( EquipmentMeshRegion blendShape in item.m_converedMeshRegions )
        {
            m_targetMesh.SetBlendShapeWeight ( ( int ) blendShape, weight );
        }
    }

    #endregion

    #region Equip Default Items

    private void EquipDefaultItem ( int slot )
    {
        Equip ( m_defaultItems [ slot ] );
    }

    private void EquipDefaultItems ( )
    {
        foreach ( Equipment item in m_defaultItems )
        {
            Equip ( item );
        }
    }

    #endregion
}