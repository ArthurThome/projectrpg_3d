﻿/// Arthur M. Thomé
/// 04 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using TMPro;

#endregion

public class InfoPlayerUI : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private Image m_healthSlider;
    [ SerializeField ] private Image m_expSlider;

    [ SerializeField ] private TextMeshProUGUI m_txtLevel;
    [ SerializeField ] private TextMeshProUGUI m_txtExp;

    [ SerializeField ] private TextMeshProUGUI m_txtCurrentHealth;
    [ SerializeField ] private TextMeshProUGUI m_txtMaxHealth;


    [ SerializeField ] private PlayerStats m_characterStats;
    [ SerializeField ] private PlayerStatsController m_playerStatsController;

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        m_characterStats.OnHealthChanged += OnHealthChanged;
        m_playerStatsController.onExpChanged += OnExpChanged;
        
        OnExpChanged ( PlayerManager._Instance.MPlayerData.GetCurrentLevel ( ), PlayerManager._Instance.MPlayerData.GetCurrentExp ( ), PlayerStatsController._Instance.GetNextLevelExp ( ) );
        OnHealthChanged ( PlayerManager._Instance.MPlayerData.health, PlayerManager._Instance.MPlayerData.GetCurrentHealth ( ) );
    }
    
    void Update ( )
    {
        
    }

    #endregion

    #region Setup / Update UI

    private void SetupUI ( )
    {

    }

    public void UpdateUI ( int level, float currentExp, float nextLevelExp )
    {
        m_txtLevel.text = "Level: " + level ;
        m_txtExp.text = "EXP: " + currentExp + " / " + nextLevelExp; 
        
        PlayerManager._Instance.MPlayerData.SetCurrentLevel ( level, currentExp );
    }

    private void UpdateHealthBar ( int maxHealth, int currentHealth )
    {
        m_txtCurrentHealth.text = currentHealth.ToString ( );
        m_txtMaxHealth.text = maxHealth.ToString ( );

        PlayerManager._Instance.MPlayerData.SetCurrentHealth ( currentHealth );
    }

    #endregion

    #region Event On Health Changed

    public void OnHealthChanged ( int maxHealth, int currentHealth )
    {
        float healthPercent = currentHealth / ( float ) maxHealth;

        m_healthSlider.fillAmount = healthPercent;

        UpdateHealthBar ( maxHealth, currentHealth );
    }

    #endregion

    #region Event On Exp Changed

    private void OnExpChanged ( int level, float currentExp, float nextLevelExp )
    {
        float expPercent = currentExp / ( float ) nextLevelExp;

        m_expSlider.fillAmount = expPercent;

        UpdateUI ( level, currentExp, nextLevelExp );

        
    }

    #endregion
}