﻿/// Arthur M. Thomé
/// 29 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class Inventory : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private List < Item > m_inventoryItems = new List < Item > ( );

    private int m_inventoryQuantity = 20;

    #endregion

    #region Properties

    public List < Item > MInventoryItems { get { return m_inventoryItems; } }

    #endregion

    #region Delegate

    public delegate void OnItemChanged ( );
    public OnItemChanged onItemChangedCallback;

    #endregion

    #region Add / Remove Item

    public bool AddItemToInventory ( Item item )
    {
        if ( !item.isDefaultItem )
        {
            if ( m_inventoryItems.Count >= m_inventoryQuantity )
            {
                Debug.Log ( "Not enough room." );
                return false;
            }

            m_inventoryItems.Add ( item );

            ItemChangedCallback ( );
        }

        return true;
    }

    public void RemoveItemFromInventory ( Item item )
    {
        m_inventoryItems.Remove ( item );

        ItemChangedCallback ( );
    }

    #endregion

    #region Callback

    public void ItemChangedCallback ( )
    {
        if ( onItemChangedCallback != null )
                onItemChangedCallback.Invoke ( );
    }

    #endregion
}