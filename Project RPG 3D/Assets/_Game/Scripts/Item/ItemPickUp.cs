﻿/// Arthur M. Thomé
/// 29 MAY 2020

#region Statements

using UnityEngine;

#endregion

public class ItemPickUp : Interactable
{
    #region Fields

    [ SerializeField ] private Item m_item;

    #endregion

    #region Override Interact

    public override void Interact ( )
    {
        base.Interact ( );

        PickUp ( );
    }

    #endregion

    #region Pick Up

    private void PickUp ( )
    {
        // Add to inventory.
        if ( GameManager._Instance.MInventory.AddItemToInventory ( m_item ) ) // Return true or false
            Destroy ( gameObject );

    }

    #endregion
}