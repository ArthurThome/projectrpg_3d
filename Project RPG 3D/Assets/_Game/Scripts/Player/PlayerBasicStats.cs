﻿/// Arthur M. Thomé
/// 05 JUN 2020
/// 
#region enum Type Character

//public enum TypeCharacter
//{
//    ENGINEER,
//    FIGHTER,
//    NERD,
//    SURVIVALIST
//}

#endregion

#region Statements

using System.IO;
using UnityEngine;

using System.Collections.Generic;

#endregion

// FAZER SISTEMA DE QUANTO MAIOR O NIVEL DO ATRIBUTO, MAIS PONTOS PRECISA PARA AUMENTAR

// juntar sistema de panel character e panel stats

[System.Serializable ]
public class PlayerBasicStats
{
    #region Fields

    #region Constants

    [ SerializeField ] private const string C_PATH = "Assets/Data/PlayerBasicStats.txt";

    #endregion

    [ SerializeField ] private int m_currentLevel;
    [ SerializeField ] private float m_currentEXP;
    [ SerializeField ] private int m_currentHealth;

    public int health;
    public int stamina;
    public int intelligence; 
    public int craftingSkill;
    public int food;
    public int water;
    public int defense;
    public int attack;
    public float weight;

    [ SerializeField ] private int m_currentAttack;
    [ SerializeField ] private int m_currentDefense;

    //[ SerializeField ] private Dictionary < string, int > m_attributes;

    [ SerializeField ] private int m_assignmentPoints;

    [ SerializeField ] private int m_attributeHealth;
    [ SerializeField ] private int m_attributeStamina;
    [ SerializeField ] private int m_attributeIntelligence;
    [ SerializeField ] private int m_attributeCraftingSkill;
    [ SerializeField ] private int m_attributeFood;
    [ SerializeField ] private int m_attributeWater;
    [ SerializeField ] private int m_attributeDefense;
    [ SerializeField ] private int m_attributeDamage;
    [ SerializeField ] private int m_attributeWeight;

    #endregion

    #region Set / Get Current Attack / Defense

    public void SetCurrentAttack ( int value )
    {
        m_currentAttack = ( byte ) value;

        SavePlayerStats ( );
    }

    public void SetCurrentDefense ( int value )
    {
        m_currentDefense = ( byte ) value;

        SavePlayerStats ( );
    }

    public int GetCurrentAttack ( )
    {
        return m_currentAttack + attack;
    }

    public int GetCurrentDefense ( )
    {
        return m_currentDefense + defense;
    }

    #endregion

    public void SetCurrentHealth ( int currentHealth )
    {
        m_currentHealth = currentHealth;

        SavePlayerStats ( );
    }

    public int GetCurrentHealth ( )
    {
        return m_currentHealth;
    }

    public void SetCurrentLevel ( int level/*, float currentExp*/ )
    {
        m_currentLevel = level;
        //SetCurrentEXP ( currentExp );
        SavePlayerStats ( );
    }

    public void SetCurrentLevel ( int level, float currentExp )
    {
        m_currentLevel = level;
        SetCurrentEXP ( currentExp );
        //SavePlayerStats ( );
    }

    public int GetCurrentLevel ( )
    {
        return m_currentLevel;
    }

    public void SetCurrentEXP ( float exp )
    {
        m_currentEXP = exp;
        SavePlayerStats ( );
    }

    public float GetCurrentExp ( )
    {
        return m_currentEXP;
    }

    public int GetAssignmentPoints ( )
    {
        return m_assignmentPoints;
    }

    public void SetAssignmentPoints ( int points )
    {
        m_assignmentPoints = points;
    }

    public void SetAttribute ( string attributeName, int value )
    {
        switch ( attributeName )
        {
            case "Health":
                m_attributeHealth += value;
                break;

            case "Stamina":
                m_attributeStamina += value;                    
                break;
                
            case "Intelligence":
                m_attributeIntelligence += value;
                break;

            case "CraftingSkill":                    
                m_attributeCraftingSkill += value;
                break;

            case "Food":
                m_attributeFood += value;
                break;

            case "Water":                    
                m_attributeWater += value;
                break;

            case "Defense":
                m_attributeDefense += value;
                break;

            case "Damage":
                m_attributeDamage += value;
                break;

            case "Weight":
                m_attributeWeight += value;
                break;

        }
    }

    public int GetAttribute ( string attributeName )
    {
        int temporario = 0;

        switch ( attributeName )
        {
            case "Health":
                temporario = m_attributeHealth;
                break;

            case "Stamina":
                temporario = m_attributeStamina;                    
                break;
                
            case "Intelligence":
                temporario = m_attributeIntelligence;
                break;

            case "CraftingSkill":                    
                temporario = m_attributeCraftingSkill;
                break;

            case "Food":
                temporario = m_attributeFood;
                break;

            case "Water":                    
                temporario = m_attributeWater;
                break;

            case "Defense":
                temporario = m_attributeDefense;
                break;

            case "Damage":
                temporario = m_attributeDamage;
                break;

            case "Weight":
                temporario = m_attributeWeight;
                break;
        }

        return temporario;
    }

    #region Save / Load

    public void SavePlayerStats ( )
    {
        Debug.Log ( "Saving File PlayerBasicStats" );
        var content = JsonUtility.ToJson ( this, true );
        File.WriteAllText ( C_PATH, content );
    }

    public void LoadPlayerStats ( )
    {
        var content = File.ReadAllText ( C_PATH );
        var p = JsonUtility.FromJson < PlayerBasicStats > ( content );

        m_currentLevel = p.m_currentLevel;
        m_currentEXP = p.m_currentEXP;
        m_currentHealth = p.m_currentHealth;
        health = p.health;
        stamina = p.stamina;
        intelligence = p.intelligence;
        craftingSkill = p.craftingSkill;
        food = p.food;
        water = p.water;
        defense = p.defense;
        attack = p.attack;
        weight = p.weight;
        m_currentAttack = p.m_currentAttack;
        m_currentDefense = p.m_currentDefense;

        Debug.Log ( "Loading File PlayerBasicStats" );
    }
    
    #endregion
}



#region Basic Stats of each Type

/*
 *  Survivalist
 * 
 *  health = 130
 *  stamina = 130
 *  intelligence = 12
 *  craftingSkill = 8
 *  food = 15
 *  water = 15
 *  defense = 2
 *  attack = 4
 *  weight = 
 *  
 *  /\/\/\/\/\/\/\/\/\
 *  
 *  FIGHTER
 *  
 *  startHealth = 100
 *  startStamina = 80
 *  strenght = 20
 *  intelligence = 7
 *  craftingSkill = 7
 *  baseDefense = 8
 *  baseAttack = 8
 *  
 *  /\/\/\/\/\/\/\/\/\
 *  
 *  NERD
 *  
 *  startHealth = 170
 *  startStamina = 50
 *  strenght = 6
 *  intelligence = 25
 *  craftingSkill = 10
 *  baseDefense = 3
 *  baseAttack = 3
 *  
 *  /\/\/\/\/\/\/\/\/\
 *  
 *  ENGINEER
 *  
 *  startHealth = 90
 *  startStamina = 100
 *  strenght = 8
 *  intelligence = 10
 *  craftingSkill = 20
 *  baseDefense = 3
 *  baseAttack = 3
 *  
 */

#endregion