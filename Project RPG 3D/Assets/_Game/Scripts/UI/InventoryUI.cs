﻿/// Arthur M. Thomé
/// 30 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class InventoryUI : MonoBehaviour
{
    #region Fields

    #region Constants

    private const string C_INVENTORY_BUTTON = "Inventory";

    #endregion

    public RectTransform m_inventoryUI = null;

    public Transform m_itemsParent = null;

    private InventorySlot [ ] m_slots;

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        m_inventoryUI.gameObject.SetActive ( false );

        GameManager._Instance.MInventory.onItemChangedCallback += UpdateUI;

        m_slots = m_itemsParent.GetComponentsInChildren < InventorySlot > ( ); //tirar esse get component!!!!
    }
    
    void Update ( )
    {
        if ( Input.GetButtonDown ( C_INVENTORY_BUTTON ) )
        {
            m_inventoryUI.gameObject.SetActive ( !m_inventoryUI.gameObject.activeSelf );
        }
    }

    #endregion

    #region Update UI

    private void UpdateUI ( )
    {
        for ( int i = 0; i < m_slots.Length; i++ )
        {
            if ( i < GameManager._Instance.MInventory.MInventoryItems.Count )
            {
                m_slots [ i ].AddItem ( GameManager._Instance.MInventory.MInventoryItems [ i ] );
            } else
            {
                m_slots [ i ].ClearSlot ( );
            }
        }
    }

    #endregion
}