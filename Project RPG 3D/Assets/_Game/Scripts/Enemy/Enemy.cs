﻿/// Arthur M. Thomé
/// 01 JUN 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

/* Handles interaction with the Enemy */
[ RequireComponent ( typeof ( CharacterStats ) ) ]
public class Enemy : Interactable
{
    #region Fields

    private CharacterStats m_myStats;

    #endregion

    #region MonoBehaviour Methods

    private void Start ( )
    {
        m_myStats = GetComponent < CharacterStats > ( );
    }

    #endregion

    #region Override Interact

    public override void Interact ( )
    {
        base.Interact ( );

        // Atack the enemy
        CharacterCombat playerCombat = PlayerManager._Instance.MCharacterCombat;

        if ( playerCombat != null )
        {
            playerCombat.Attack ( m_myStats );
        }
    }

    #endregion
}