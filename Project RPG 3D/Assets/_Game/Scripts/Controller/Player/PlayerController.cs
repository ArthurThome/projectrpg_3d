﻿/// Arthur M. Thomé
/// 29 MAY 2020

#region Statements

using UnityEngine;
using UnityEngine.EventSystems;

#endregion

[ RequireComponent ( typeof ( PlayerMotor ) ) ]
public class PlayerController : MonoBehaviour
{
    #region Fields

    //[ SerializeField ] private PlayerMotor m_instancePlayerMotor = null;

    [ SerializeField ] private Interactable m_focus;

    [ SerializeField ] private LayerMask m_layerGround;
    private Camera m_mainCam;

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        m_mainCam = Camera.main;
    }
    
    void Update ( )
    {
        if ( EventSystem.current.IsPointerOverGameObject ( ) )
            return;

        // If we Press Left mouse
        if ( Input.GetMouseButtonDown ( 0 ) )
        {
            Ray ray = m_mainCam.ScreenPointToRay ( Input.mousePosition );
            RaycastHit hit;

            if ( Physics.Raycast ( ray, out hit, 100, m_layerGround ) )
            {
                // Move our players to what we hit
                GameManager._Instance.MPlayerMotor.MoveToPoint ( hit.point );

                // Stop focusing any objects
                RemoveFocus ( );
            }
        }

        // If we Press Right mouse
        if ( Input.GetMouseButtonDown ( 1 ) )
        {
            // We create a ray
            Ray ray = m_mainCam.ScreenPointToRay ( Input.mousePosition );
            RaycastHit hit;

            // If the ray hits
            if ( Physics.Raycast ( ray, out hit, 100 ) )
            {
                Interactable interactable = hit.collider.GetComponent < Interactable > ( );
                if ( interactable != null )
                {
                    SetFocus ( interactable );
                }
            }
        }
    }

    #endregion

    private void SetFocus ( Interactable newFocus )
    {
        if ( newFocus != m_focus )
        {
            if ( m_focus != null )
                m_focus.OnDefocused ( );

            m_focus = newFocus;
            GameManager._Instance.MPlayerMotor.FollowTarget ( newFocus );
        }

        newFocus.OnFocused ( transform );
    }

    private void RemoveFocus ( )
    {
        if ( m_focus != null )
            m_focus.OnDefocused ( );

        m_focus = null;
        GameManager._Instance.MPlayerMotor.StopFollowingTarget ( );
    }
}