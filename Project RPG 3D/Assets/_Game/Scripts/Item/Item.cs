﻿/// Arthur M. Thomé
/// 29 MAY 2020

#region Statements

using UnityEngine;

#endregion

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    #region Fields

    new public string name = "New Item";
    public Sprite icon = null;
    public bool isDefaultItem = false;

    #endregion

    #region Use Item

    public virtual void Use()
    {
        // Use the item
        // Something might happen

        //Debug.Log("Using " + name);
    }

    #endregion

    #region Remove from Inventory

    public void RemoveFromInventory ( )
    {
        GameManager._Instance.MInventory.RemoveItemFromInventory ( this );
    }

    #endregion
}